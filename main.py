import base64
import os
from fastapi import FastAPI, File, UploadFile
from ultralytics import YOLO
import cv2
import numpy as np

app = FastAPI()

model = YOLO()

@app.post("/predict")
async def infer_image(image: UploadFile = File(...)):
    contents = await image.read()
    image_np = np.frombuffer(contents, dtype=np.uint8)
    img = cv2.imdecode(image_np, cv2.IMREAD_COLOR)

    results = model.predict(img, save=True, imgsz=320, conf=0.5)[0]

    save_path = os.path.join(results.save_dir, results.path)
    with open(save_path, "rb") as image_file:
        encoded_image = base64.b64encode(image_file.read()).decode('utf-8')

    os.remove(save_path)

    return {"results": results.tojson(), "encoded_image": encoded_image}
